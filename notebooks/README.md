# Notes on preliminary data analysis for the PPS pumps data - Paolo Andrich

In December 2021 HAL24K Water initiated its work with the company PPS pumps, which produces sump pumps used to remove water that accumulated in collection basins usually located in the basement of homes. These basins can be associated to either foul water coming from bathrooms or swimming pools placed in the underground levels of a building or to ground water that can accumulate due, for instance, to high precipitation levels. 

According to the information that was received by PPS pumps, fould water pump systems consist of a single pump unit while ground water pumps have two complementary units. At the company [website](https://support.ppspumps.com/hc/en-gb/articles/360007875400-Duty-Standby-Dual-Foul-Water-Pump-Station-with-Rego1-Wiring-Diagram) we see that they do also have a dual foul water system. Note that this might explain why one of the foul water dataset we have access to appears to have data from two units. 


## Data

In brief, the data received from PPS pumps included: 

- Data concerning 8 different pumps, some being ground water and some foul water systems
- The data covers different ranges of time for the different pumps:
    - Coverage varies between ~1 year and roughly 4 years
    - It's important to note that the monitoring capabilities are commonly added at various points of a pump lifetime: we do not have a picture of the whole pump lifecycle for all the pumps
- The variables that are consistently reported (i.e. they are not mostly missing) and that appear relevant for our analysis are:
    - The pump cycle length (and related pump cycle rate)
    - Electric current in the pump units
    - Voltage applied to the pump units
    - Electric resistance observed in the pump units
    - Temperature
    - Information on battery status

In addition, PPS pumps provided a label for 119 data points that are identified as "of interest". It should be noted that 117 of these refer to a single event for a single pump (#1A5008_GW). PPS provided a note for this pump which reads 

> Battery backup issues spans a period of time, labeled as PMX, might have affected voltage of the batteries (usual value 26V)

The remaining "highlighted" points refer to two separate time stamps for another pump (#1D4136_FW). For this PPS provides the following note:

> Blockage found in pump causing current to spike


## Goal

PPS pumps would like to be able to identify patterns in the variable behavior that lead to pump failures.


## Code Organization

As a result of issues in generating the required Collaborate credentials and access to the Atlas database we elected to work with the data downloaded locally on our personal machine. In particular, we used the datasets labeled as `unit_history_formatted` by Patrick and stored in the Teams channel. Code to establish connection with the database and query of the data will need to be added. Note that due to the large size of the dataset batch-querying or separate queries for the different pumps might be preferable. 

The code associated to the data analysis that was performed can be found on Bitbucket as part of the [ppspumps repository](https://bitbucket.org/hal24000/ppspumps/src/pa-exploration/). The `pa-exploration` branch was used throughout this phase of the project. 

The notebook `PA-check_datasets.ipynb` was used to perform a preliminary check of the data. Here we take a quick look at all data sets to get familiar with their structure, identify possible anomalies, and establish a method for the cleaning and preparation of the data. 

The notebook `PA-data_exploration.ipynb` provides a template for a more standardized exploration of the data where we implement the lessons learned from the initial exploration to create functions dedicated to the cleaning, preparation and visualization of the data. Figures generated within this notebook are saved at `outputs/images`.

The notebook `PA-data_exploration_launcher.ipynb` consists of a papermill wrapper function that allows one to run the `PA-data_exploration.ipynb` notebook for any pump identifier of interest. The notebook for each requested pump ID is saved in `outputs/processed_notebooks`.

## Insights

### Preliminary data check

- Data has entries for a third pump unit. This is usually not used but it is important to determine if any of the existing units could be connected this third sensor or if it can be safely disregarded.
- We believe that float timestamps refer to times when the float status changed. The time stamp appear to be repeated throughout all measurements for which the status remains changed.
- temp chamber, water height, status, offreason, ovclimit, actual_pressure, pressure_limit, offset_pressure are all missing variables.
- temperature information is often missing or covers only limited ranges of time.
- Time separation between consecutive measurements could be as short as 1 second (as expected) but also much longer than 15 minutes. It should be checked if this is the result of drops in the wifi connection (are measurements recorded only when the wifi is active or are the buffer capabilities on the sensors). Some pumps show particularly significant activity at very short and long time separations: can this indicate an anomalous behavior (for instance heavier pump use).
- As mentioned above almost all highlighted timestamps are associated to a single pump. Only one other pump has other events of interest.

### Detailed visual investigation

- When two pump units are present it is pretty clear that (main) activity switches back and forth between them in time. This is clearly seen when looking at the cumulative number of cycles perfomed by the two units. Cameron from PPS confirmed that of the two units one is usually the primary one and one is the secondary, which kicks in only if the water surpasses a certain threshold level. However, the role of primary and secondary pump is switched between the units from time to time. It is important to keep this in mind particularly if one goal is to determine the time scale for the failure of either pump.
- It is unclear what the main highlighted event refers to (pump #1A5008_GW). It seems clear that some form of maintenance was performed around this time (the behavior of most variables visibly change before and after) but it is not possible to identify the signature of a possible failure.
- The only other two highlighted events (pump #1D4136_FW) show a completely different behavior and nothing anomalous stands out. Unclear what these time of interest refer to. No spike in current is observed (which PPS seem to expect).
- A possible general behavior for the resistance seems to emerge:
    - When the resistance is high (> 60 MOhm) the recorded values jump between two-three levels. According to Cameron at PPS this might be due to the fact that when the resistance is above some threshold values are actually recorded less frequently and with a much worse resolution.
    - When the resistance dips below a value of roughly 60 MOhm its records become finer and more accurate. A transition towards a trend of progressive decay is visible.
  This transition to an exponential decay and insights on its speed seem to be one of the most interesting aspects for PPS.
- One additional observation we made is that the voltage applied to the pump unit can show a rather large variance in certain cases, to the point that its values dips below the safe threshold of 216V. According to PPS, monitoring this type of anomalous behavior that could result in additional unwanted stress on the pump units could be of interest. 
- Unfortunately we do not have reliable access to many variables that could be, at face value, very important to determine the operational status of the pumps. These are temperature, pressure, and water height.
- It might be important to determine the effect of drops in the wifi signal on the recorded data, particularly when it comes to the calculation of the cycle rate.
- What is the cause of the very long time that occurs between some data entries? This needs to be further investigated, it could also be that we are not analysing the data properly.


## Final comments

- Current data is extremely limited: 
    - We essentially have only one event of interest for which we have no information
    - It is unclear what constitutes a normal and an abnormal behavior
    - Possibly PPS is interested in determine the resistance decay but further clarity needs to be provided concerning the patterns of interest
- Future outlook: 
    - PPS should provide maintenance logs
    - PPS needs to provide a more extensive log of recorded issues and a better label for a faulty behavior
    - Access to the data for many more pumps is required to start establishing possible patterns. In particular it might be worth focusing on pumps for which the data covers a large span of the pump lifecycle. This way the evolution of the pump resistance might be tracked and better analyzed. 
    - PPS will provide partial geographic information that might be helpful as more pumps are considered.
